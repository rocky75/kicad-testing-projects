EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 700  1100 1050 500 
U 5FA49B6B
F0 "XC6SLX45T_A" 50
F1 "spartan6_XC6SLX45T_A.sch" 50
$EndSheet
$Sheet
S 1900 1100 1050 500 
U 5FA6AAAC
F0 "XC6SLX45T_B" 50
F1 "spartan6_XC6SLX45T_B.sch" 50
$EndSheet
$Sheet
S 700  1950 1050 500 
U 5FAA35AC
F0 "XC6SLX45T_C" 50
F1 "spartan6_XC6SLX45T_C.sch" 50
$EndSheet
$Sheet
S 1900 1950 1050 500 
U 5FAA415F
F0 "XC6SLX45T_D" 50
F1 "spartan6_XC6SLX45T_D.sch" 50
$EndSheet
$Sheet
S 700  2850 1050 500 
U 5FAA4937
F0 "XC6SLX45T_E" 50
F1 "spartan6_XC6SLX45T_E.sch" 50
$EndSheet
$Sheet
S 1900 2850 1050 500 
U 5FAA5116
F0 "XC6SLX45T_F_G" 50
F1 "spartan6_XC6SLX45T_F_G.sch" 50
$EndSheet
$Sheet
S 4150 1200 1050 500 
U 5FAA58F5
F0 "spartan6_G" 50
F1 "spartan6_G.sch" 50
$EndSheet
Text Notes 800  900  0    50   ~ 0
XC6SLX45T-3FGG484C XILINX FPGA (SPARTAN 6 DSP)
$EndSCHEMATC
