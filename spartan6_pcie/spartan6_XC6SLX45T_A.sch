EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eec:XC6SLX45T-3FGG484C U1
U 1 1 5FA5FAF1
P 4750 800
AR Path="/5FA49B6B/5FA5FAF1" Ref="U1"  Part="1" 
AR Path="/5FA6AAAC/5FA5FAF1" Ref="U?"  Part="1" 
F 0 "U1" H 6878 -2504 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 6878 -2595 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 4750 1200 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 4750 1300 50  0001 L CNN
F 4 "510-00002" H 4750 1400 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 4750 1500 50  0001 L CNN "RAM size"
F 6 "+150°C" H 4750 1600 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 4750 1700 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 4750 1800 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 4750 1900 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 4750 2000 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 4750 2100 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 4750 2200 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 4750 2300 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 4750 2400 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 4750 2500 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 4750 2600 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 4750 2700 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 4750 2800 50  0001 L CNN "library id"
F 19 "Xilinx" H 4750 2900 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 4750 3000 50  0001 L CNN "max junction temp"
F 21 "RAM" H 4750 3100 50  0001 L CNN "memory type"
F 22 "296" H 4750 3200 50  0001 L CNN "number of I/Os"
F 23 "3411" H 4750 3300 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 4750 3400 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 4750 3500 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 4750 3600 50  0001 L CNN "package"
F 27 "yes" H 4750 3700 50  0001 L CNN "rohs"
F 28 "3" H 4750 3800 50  0001 L CNN "speed grade"
F 29 "+85°C" H 4750 3900 50  0001 L CNN "temperature range high"
F 30 "0°C" H 4750 4000 50  0001 L CNN "temperature range low"
	1    4750 800 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x18_PCIe_mini_x1_edge J??
U 1 1 5FAC5D9F
P 9400 2700
F 0 "J??" H 9400 3725 50  0000 C CNN
F 1 "Conn_02x18_PCIe_mini_x1_edge" H 9400 3634 50  0000 C CNN
F 2 "Connector_PCBEdge:BUS_PCIexpress_x1" H 9300 3700 50  0001 C CNN
F 3 "http://www.ritrontek.com/uploadfile/2016/1026/20161026105231124.pdf#page=63" H 9200 2800 50  0001 C CNN
	1    9400 2700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
