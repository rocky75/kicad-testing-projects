EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eec:XC6SLX45T-3FGG484C U?
U 6 1 5FBA9D54
P 4400 1600
F 0 "U?" H 6528 746 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 6528 655 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 4400 2000 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 4400 2100 50  0001 L CNN
F 4 "510-00002" H 4400 2200 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 4400 2300 50  0001 L CNN "RAM size"
F 6 "+150°C" H 4400 2400 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 4400 2500 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 4400 2600 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 4400 2700 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 4400 2800 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 4400 2900 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 4400 3000 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 4400 3100 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 4400 3200 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 4400 3300 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 4400 3400 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 4400 3500 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 4400 3600 50  0001 L CNN "library id"
F 19 "Xilinx" H 4400 3700 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 4400 3800 50  0001 L CNN "max junction temp"
F 21 "RAM" H 4400 3900 50  0001 L CNN "memory type"
F 22 "296" H 4400 4000 50  0001 L CNN "number of I/Os"
F 23 "3411" H 4400 4100 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 4400 4200 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 4400 4300 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 4400 4400 50  0001 L CNN "package"
F 27 "yes" H 4400 4500 50  0001 L CNN "rohs"
F 28 "3" H 4400 4600 50  0001 L CNN "speed grade"
F 29 "+85°C" H 4400 4700 50  0001 L CNN "temperature range high"
F 30 "0°C" H 4400 4800 50  0001 L CNN "temperature range low"
	6    4400 1600
	1    0    0    -1  
$EndComp
$Comp
L eec:XC6SLX45T-3FGG484C U?
U 7 1 5FBBC171
P 4450 4450
F 0 "U?" H 6578 3596 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 6578 3505 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 4450 4850 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 4450 4950 50  0001 L CNN
F 4 "510-00002" H 4450 5050 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 4450 5150 50  0001 L CNN "RAM size"
F 6 "+150°C" H 4450 5250 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 4450 5350 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 4450 5450 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 4450 5550 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 4450 5650 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 4450 5750 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 4450 5850 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 4450 5950 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 4450 6050 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 4450 6150 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 4450 6250 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 4450 6350 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 4450 6450 50  0001 L CNN "library id"
F 19 "Xilinx" H 4450 6550 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 4450 6650 50  0001 L CNN "max junction temp"
F 21 "RAM" H 4450 6750 50  0001 L CNN "memory type"
F 22 "296" H 4450 6850 50  0001 L CNN "number of I/Os"
F 23 "3411" H 4450 6950 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 4450 7050 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 4450 7150 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 4450 7250 50  0001 L CNN "package"
F 27 "yes" H 4450 7350 50  0001 L CNN "rohs"
F 28 "3" H 4450 7450 50  0001 L CNN "speed grade"
F 29 "+85°C" H 4450 7550 50  0001 L CNN "temperature range high"
F 30 "0°C" H 4450 7650 50  0001 L CNN "temperature range low"
	7    4450 4450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
