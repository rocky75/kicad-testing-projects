EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pcie_spartan6_card-rescue:XC6SLX45T-3FGG484C-eec U1
U 6 1 5FBA9D54
P 5500 1350
F 0 "U1" H 7628 496 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 7628 405 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 5500 1750 50  0000 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 5500 1850 50  0001 L CNN
F 4 "510-00002" H 5500 1950 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 5500 2050 50  0001 L CNN "RAM size"
F 6 "+150°C" H 5500 2150 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 5500 2250 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 5500 2350 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 5500 2450 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 5500 2550 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 5500 2650 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 5500 2750 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 5500 2850 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 5500 2950 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 5500 3050 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 5500 3150 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 5500 3250 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 5500 3350 50  0001 L CNN "library id"
F 19 "Xilinx" H 5500 3450 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 5500 3550 50  0001 L CNN "max junction temp"
F 21 "RAM" H 5500 3650 50  0001 L CNN "memory type"
F 22 "296" H 5500 3750 50  0001 L CNN "number of I/Os"
F 23 "3411" H 5500 3850 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 5500 3950 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 5500 4050 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 5500 4150 50  0001 L CNN "package"
F 27 "yes" H 5500 4250 50  0001 L CNN "rohs"
F 28 "3" H 5500 4350 50  0001 L CNN "speed grade"
F 29 "+85°C" H 5500 4450 50  0001 L CNN "temperature range high"
F 30 "0°C" H 5500 4550 50  0001 L CNN "temperature range low"
	6    5500 1350
	1    0    0    -1  
$EndComp
$Comp
L pcie_spartan6_card-rescue:XC6SLX45T-3FGG484C-eec U1
U 7 1 5FBBC171
P 5550 4200
F 0 "U1" H 7678 3346 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 7678 3255 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 5550 4600 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 5550 4700 50  0001 L CNN
F 4 "510-00002" H 5550 4800 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 5550 4900 50  0001 L CNN "RAM size"
F 6 "+150°C" H 5550 5000 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 5550 5100 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 5550 5200 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 5550 5300 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 5550 5400 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 5550 5500 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 5550 5600 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 5550 5700 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 5550 5800 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 5550 5900 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 5550 6000 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 5550 6100 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 5550 6200 50  0001 L CNN "library id"
F 19 "Xilinx" H 5550 6300 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 5550 6400 50  0001 L CNN "max junction temp"
F 21 "RAM" H 5550 6500 50  0001 L CNN "memory type"
F 22 "296" H 5550 6600 50  0001 L CNN "number of I/Os"
F 23 "3411" H 5550 6700 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 5550 6800 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 5550 6900 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 5550 7000 50  0001 L CNN "package"
F 27 "yes" H 5550 7100 50  0001 L CNN "rohs"
F 28 "3" H 5550 7200 50  0001 L CNN "speed grade"
F 29 "+85°C" H 5550 7300 50  0001 L CNN "temperature range high"
F 30 "0°C" H 5550 7400 50  0001 L CNN "temperature range low"
	7    5550 4200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
