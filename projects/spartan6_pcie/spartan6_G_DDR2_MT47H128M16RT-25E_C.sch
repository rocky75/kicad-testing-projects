EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MT47H128M16RT-25E:MT47H128M16RT-25E_C U?
U 1 1 5FDD2E34
P 4600 2100
F 0 "U?" H 5600 2487 60  0000 C CNN
F 1 "MT47H128M16RT-25E_C" H 5600 2381 60  0000 C CNN
F 2 "FBGA84_9X12P5_MRN" H 5600 2340 60  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Micron%20Technology%20Inc%20PDFs/MT47H256M8_MT47H128M16_RevF_2018.pdf" H 4600 2100 60  0001 C CNN
	1    4600 2100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
