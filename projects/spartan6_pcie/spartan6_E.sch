EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 6 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L eec:XC6SLX45T-3FGG484C U?
U 5 1 5FB737A8
P 12150 14850
F 0 "U?" H 13192 1885 50  0000 C CNN
F 1 "XC6SLX45T-3FGG484C" H 13192 1976 50  0000 C CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 12150 15250 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 12150 15350 50  0001 L CNN
F 4 "510-00002" H 12150 15450 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 12150 15550 50  0001 L CNN "RAM size"
F 6 "+150°C" H 12150 15650 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 12150 15750 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 12150 15850 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 12150 15950 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 12150 16050 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 12150 16150 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 12150 16250 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 12150 16350 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 12150 16450 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 12150 16550 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 12150 16650 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 12150 16750 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 12150 16850 50  0001 L CNN "library id"
F 19 "Xilinx" H 12150 16950 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 12150 17050 50  0001 L CNN "max junction temp"
F 21 "RAM" H 12150 17150 50  0001 L CNN "memory type"
F 22 "296" H 12150 17250 50  0001 L CNN "number of I/Os"
F 23 "3411" H 12150 17350 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 12150 17450 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 12150 17550 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 12150 17650 50  0001 L CNN "package"
F 27 "yes" H 12150 17750 50  0001 L CNN "rohs"
F 28 "3" H 12150 17850 50  0001 L CNN "speed grade"
F 29 "+85°C" H 12150 17950 50  0001 L CNN "temperature range high"
F 30 "0°C" H 12150 18050 50  0001 L CNN "temperature range low"
	5    12150 14850
	-1   0    0    1   
$EndComp
$EndSCHEMATC
