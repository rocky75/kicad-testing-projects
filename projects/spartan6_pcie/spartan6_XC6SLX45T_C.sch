EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 4 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pcie_spartan6_card-rescue:XC6SLX45T-3FGG484C-eec U1
U 3 1 5FAC2722
P 13600 2000
F 0 "U1" V 15865 -4050 50  0000 C CNN
F 1 "XC6SLX45T-3FGG484C" V 15774 -4050 50  0000 C CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 13600 2400 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 13600 2500 50  0001 L CNN
F 4 "510-00002" H 13600 2600 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 13600 2700 50  0001 L CNN "RAM size"
F 6 "+150°C" H 13600 2800 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 13600 2900 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 13600 3000 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 13600 3100 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 13600 3200 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 13600 3300 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 13600 3400 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 13600 3500 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 13600 3600 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 13600 3700 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 13600 3800 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 13600 3900 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 13600 4000 50  0001 L CNN "library id"
F 19 "Xilinx" H 13600 4100 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 13600 4200 50  0001 L CNN "max junction temp"
F 21 "RAM" H 13600 4300 50  0001 L CNN "memory type"
F 22 "296" H 13600 4400 50  0001 L CNN "number of I/Os"
F 23 "3411" H 13600 4500 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 13600 4600 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 13600 4700 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 13600 4800 50  0001 L CNN "package"
F 27 "yes" H 13600 4900 50  0001 L CNN "rohs"
F 28 "3" H 13600 5000 50  0001 L CNN "speed grade"
F 29 "+85°C" H 13600 5100 50  0001 L CNN "temperature range high"
F 30 "0°C" H 13600 5200 50  0001 L CNN "temperature range low"
	3    13600 2000
	1    0    0    -1  
$EndComp
$EndSCHEMATC
