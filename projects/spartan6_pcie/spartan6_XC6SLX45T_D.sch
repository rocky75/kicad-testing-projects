EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 5 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pcie_spartan6_card-rescue:XC6SLX45T-3FGG484C-eec U1
U 4 1 5FB51720
P 13350 2050
F 0 "U1" H 15478 -4154 50  0000 L CNN
F 1 "XC6SLX45T-3FGG484C" H 15478 -4245 50  0000 L CNN
F 2 "Xilinx-XC6SLX45T-3FGG484C-0-0-*" H 13350 2450 50  0001 L CNN
F 3 "https://www.xilinx.com/support/documentation/data_sheets/ds162.pdf" H 13350 2550 50  0001 L CNN
F 4 "510-00002" H 13350 2650 50  0001 L CNN "3DS Part Number"
F 5 "33.408KB" H 13350 2750 50  0001 L CNN "RAM size"
F 6 "+150°C" H 13350 2850 50  0001 L CNN "ambient temperature range high"
F 7 "-65°C" H 13350 2950 50  0001 L CNN "ambient temperature range low"
F 8 "IC" H 13350 3050 50  0001 L CNN "category"
F 9 "Integrated Circuits (ICs)" H 13350 3150 50  0001 L CNN "device class L1"
F 10 "Embedded Processors and Controllers" H 13350 3250 50  0001 L CNN "device class L2"
F 11 "FPGAs - Field Programmable Gate Arrays" H 13350 3350 50  0001 L CNN "device class L3"
F 12 "IC FPGA 296 I/O 484FBGA" H 13350 3450 50  0001 L CNN "digikey description"
F 13 "122-1737-ND" H 13350 3550 50  0001 L CNN "digikey part number"
F 14 "https://www.xilinx.com/support/documentation/user_guides/ug475_7Series_Pkg_Pinout.pdf" H 13350 3650 50  0001 L CNN "footprint url"
F 15 "2.6mm" H 13350 3750 50  0001 L CNN "height"
F 16 "BGA484N100P22X22_2300X2300X220" H 13350 3850 50  0001 L CNN "ipc land pattern name"
F 17 "yes" H 13350 3950 50  0001 L CNN "lead free"
F 18 "17105ea8e2290cff" H 13350 4050 50  0001 L CNN "library id"
F 19 "Xilinx" H 13350 4150 50  0001 L CNN "manufacturer"
F 20 "+125°C" H 13350 4250 50  0001 L CNN "max junction temp"
F 21 "RAM" H 13350 4350 50  0001 L CNN "memory type"
F 22 "296" H 13350 4450 50  0001 L CNN "number of I/Os"
F 23 "3411" H 13350 4550 50  0001 L CNN "number of logic blocks"
F 24 "43661" H 13350 4650 50  0001 L CNN "number of logic elements cells"
F 25 "54576" H 13350 4750 50  0001 L CNN "number of registers"
F 26 "FBGA484" H 13350 4850 50  0001 L CNN "package"
F 27 "yes" H 13350 4950 50  0001 L CNN "rohs"
F 28 "3" H 13350 5050 50  0001 L CNN "speed grade"
F 29 "+85°C" H 13350 5150 50  0001 L CNN "temperature range high"
F 30 "0°C" H 13350 5250 50  0001 L CNN "temperature range low"
	4    13350 2050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
