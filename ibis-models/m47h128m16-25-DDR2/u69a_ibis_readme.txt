U69A IBIS MODEL 
-----------------
Device: 2Gb DDR-2 SDRAM - Die Revision "C" 
2Gbb DDR2 SDRAM IBIS models
Rev 1.0: 05/07/2010
Rev 1.1: 05/25/2010.
Rev 1.2: 07/16/2010
Rev 2.0: 09/03/2010
Rev 2.1: 09/27/2010
Rev 2.2: 06/23/2011
Rev 2.3: 07/10/2013
	-See detailed notes on revisions in u69a.ibs, u69a_it.ibs, and u69a_at.ibs.

u69a.ibs is applicable to Commercial temperature range (0C to 85C)
u69a_it.ibs is applicable to Industrial temperature range (-40C to 95C)
u69a_it.ibs is applicable to Industrial temperature range (-40C to 105C)

This IBIS model includes On Die Termination (ODT) characteristics.  ODT is
modeled through the use of [Submodel] power and ground clamp I-V curves that
add the termination characteristics to the regular power and ground clamp
characteristics when the I/O is functioning as an input.  ODT applies to the
DQ, DQS/DQS#, and DM signals.  Use the [Model Selector] to choose between 50,
75, or 150 ohm ODT settings.

Models for 400, 533, 667, 800, and 1067  speed grades included and selected 
through [Model Selector]. Models *_533 are applicable for speed grade 400/533 Mbps, 
models *_667 are for speed grade 667 Mbps, models *_800 are applicable for speed 
grade 800 Mbps, and  models *_1067 are applicable for speed grade 1067 Mbps.

Model data is extracted from the HSPICE model, revision 2.2.

NOTE: C_comp is lower for the ODT-enabled models versus the normal I/O model.
The C_comp value is set to match the input state of the DQ I/O as measured from
the HSPICE model.  Due to the C_comp differences between the DQ model and the
DQ_ODT* models, the output characteristics of the models will look different
when simulated.  So, to match most closely with the HSPICE model, simulate with
the DQ_FULL or DQ_HALF model for ALL Output simulations.  Use the ODT models
ONLY for Input simulations. 

[Disclaimer]  This software code and all associated documentation, comments
              or other information (collectively "Software") is provided
              "AS IS" without warranty of any kind. MICRON TECHNOLOGY, INC.
              ("MTI") EXPRESSLY DISCLAIMS ALL WARRANTIES EXPRESS OR IMPLIED,
              INCLUDING BUT NOT LIMITED TO, NONINFRINGEMENT OF THIRD PARTY
              RIGHTS, AND ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR
              FITNESS FOR ANY PARTICULAR PURPOSE. MTI DOES NOT WARRANT THAT 
              THE SOFTWARE WILL MEET YOUR REQUIREMENTS, OR THAT THE 
              OPERATION OF THE SOFTWARE WILL BE UNINTERRUPTED OR ERROR-FREE. 
              FURTHERMORE, MTI DOES NOT MAKE ANY REPRESENTATIONS REGARDING
              THE USE OR THE RESULTS OF THE USE OF THE SOFTWARE IN TERMS OF
              ITS CORRECTNESS, ACCURACY, RELIABILITY, OR OTHERWISE. THE
              ENTIRE RISK ARISING OUT OF USE OR PERFORMANCE OF THE SOFTWARE
              REMAINS WITH YOU. IN NO EVENT SHALL MTI, ITS AFFILIATED
              COMPANIES OR THEIR SUPPLIERS BE LIABLE FOR ANY DIRECT,
              INDIRECT, CONSEQUENTIAL, INCIDENTAL, OR SPECIAL DAMAGES
              (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS,
              BUSINESS INTERRUPTION, OR LOSS OF INFORMATION) ARISING OUT OF
              YOUR USE OF OR INABILITY TO USE THE SOFTWARE, EVEN IF MTI HAS
              BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. Because some
              jurisdictions prohibit the exclusion or limitation of 
              liability for consequential or incidental damages, the above
              limitation may not apply to you.
 
[Copyright]   Copyright 2013 Micron Technology, Inc. All rights reserved.
